#include "netex_stopplace.h"

NETEX_STOPPLACE::NETEX_STOPPLACE()
{
    // Dies ist der Standard-Konstruktor
}

NETEX_STOPPLACE::NETEX_STOPPLACE(string _ID, string _DHID_Code, string _Name, double _Laengengrad, double _Breitengrad)           // Dies ist ein überladener Konstruktor

{
    // Dies ist ein ueberlandender Konstruktor
    this->ID = _ID;
    this->DHID_Code = _DHID_Code;
    this->Name = _Name;
    this->Breitengrad = _Breitengrad;
    this->Laengengrad = _Laengengrad;
}

NETEX_STOPPLACE::~NETEX_STOPPLACE()
{
    // Dies ist der Standard Destruktor
}

string NETEX_STOPPLACE::Lese_ID(void)
{
    return this->ID;
}

string NETEX_STOPPLACE::Lese_DHID_Code(void)
{
    return this->DHID_Code;
}

string NETEX_STOPPLACE::Lese_Name(void)
{
    return this->Name;
}

double NETEX_STOPPLACE::Lese_Breitengrad(void)
{
    return this->Breitengrad;
}

double NETEX_STOPPLACE::Lese_Laengengrad(void)
{
    return this->Laengengrad;
}

