#ifndef NETEX_STOPPLACE_H_INCLUDED
#define NETEX_STOPPLACE_H_INCLUDED

#include <string>               // wir brauchen die Bibliothek string, um mit dem Dateityp string arbeiten
                                // zu k�nnen

using namespace std;

class NETEX_STOPPLACE {

    // Attribute
    string ID;
    string DHID_Code;
    string Name;
    double Laengengrad;
    double Breitengrad;

    // die Methoden k�nnen in allen anderen Programmteilen genutzt werden
    public:

    // Methoden
    NETEX_STOPPLACE();           // Dies ist der Standard-Konstruktor
    NETEX_STOPPLACE(string _ID, string _DHID_Code, string _Name, double _Laengengrad, double _Breitengrad);           // Dies ist ein �berladener Konstruktor
    ~NETEX_STOPPLACE();          // Dies ist der Standard Destruktor

    string Lese_ID(void);
    string Lese_DHID_Code(void);
    string Lese_Name(void);
    double Lese_Breitengrad(void);
    double Lese_Laengengrad(void);
};

#endif // NETEX_STOPPLACE_H_INCLUDED
