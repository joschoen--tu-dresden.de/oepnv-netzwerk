#include <iostream>
#include "oepnv-netzwerk.h"

OEPNV_NETZWERK::OEPNV_NETZWERK()
{
    // Dies ist der Standard-Konstruktor
    this->Anzahl_NeTEx_Dateien = 0;
    this->Netzwerk_Name = "Netzwerkname ist noch nicht gesetzt";
    this->Anzahl_NeTEx_Stopplaces = 0;
}

OEPNV_NETZWERK::~OEPNV_NETZWERK()
{
    // Dies ist der Standard Destruktor

    // falls NeTEx-Dateinamen gespeichert sind, so l�sche diese
    if( this->Anzahl_NeTEx_Dateien > 0 )
    {
        delete[] this->NeTEx_Datei;
        this->Anzahl_NeTEx_Dateien = 0;
    }
}

void OEPNV_NETZWERK::Setze_NetzwerkName(string _Netzwerk_Name)
{
    // setzt den Netzwerknamen
    this->Netzwerk_Name = _Netzwerk_Name;
}

void OEPNV_NETZWERK::Setze_Anzahl_Netex_Dateien(int _Anzahl)
{
    // setzt das Attribut Anzahl_NeTEx_Dateien

    // falls schon eine Anzahl festgelegt wurde: loesche zun�chst das bestehende Array
    if( this->Anzahl_NeTEx_Dateien > 0 )
        delete[] this->NeTEx_Datei;

    // aktualisiere die Anzahl der NeTEx-Dateien
    this->Anzahl_NeTEx_Dateien = _Anzahl;

    // Richte das Array ein
    this->NeTEx_Datei = new string [this->Anzahl_NeTEx_Dateien];
}

void OEPNV_NETZWERK::Setze_Anzahl_NeTEx_Stopplaces(int _Anzahl)
{
    // setzt das Attribut Anzahl_NeTEx_Stopplaces

    // falls schon eine Anzahl festgelegt wurde: loesche zun�chst das bestehende Array
    if( this->Anzahl_NeTEx_Stopplaces > 0 )
        delete[] this->Stop_Place;

    // aktualisiere die Anzahl der NeTEx-Dateien
    this->Anzahl_NeTEx_Stopplaces = _Anzahl;

    // Richte das Array ein
    this->Stop_Place = new class NETEX_STOPPLACE [this->Anzahl_NeTEx_Stopplaces];
}

string OEPNV_NETZWERK::Lese_NetzwerkName(void)
{
    // gibt den Netzwerknamen zur�ck
    return this->Netzwerk_Name;
}

int OEPNV_NETZWERK::Lese_Anzahl_Netex_Dateien(void)
{
    // gibt den Wert des Attributs Anzahl_NeTEx_Dateien zur�ck
    return this->Anzahl_NeTEx_Dateien;
}

int OEPNV_NETZWERK::Lese_Anzahl_Netex_Stopplaces(void)
{
    // gibt den Wert des Attributs Anzahl_NeTEx_Stopplaces zur�ck
    return this->Anzahl_NeTEx_Stopplaces;
}

void OEPNV_NETZWERK::Setze_NeTEx_Dateiname_ID(int _ID, string _DateiName)
{
    // setzt den Wert des Attributs NeTEx-Datei[_ID] auf _DateiName
    this->NeTEx_Datei[_ID] = _DateiName;
}

string OEPNV_NETZWERK::Lese_NeTEx_Dateiname_ID(int _ID)
{
    // liefert den Namen von NeTEx-Datei[_ID] zurueck
    if( _ID < this->Anzahl_NeTEx_Dateien )
        return this->NeTEx_Datei[_ID];
    else
        return "Datei gibt es nicht!";
}

void OEPNV_NETZWERK::Setze_StopPlace_ID(int _ID, class NETEX_STOPPLACE _SP)
{
    // setzt Stop_Place[_ID] = _SP
    this->Stop_Place[_ID] = _SP;
}

class NETEX_STOPPLACE OEPNV_NETZWERK::Lese_StopPlace_ID(int _ID)
{
    // gibt Stop_Place[_ID] zur�ck
    return this->Stop_Place[_ID];
}

void OEPNV_NETZWERK::Ausgabe_Netzwerk_Informationen(void)
{
    // druckt alle gespeicherten Netzwerk_Informationen am Bildschirm aus

    cout << "Netzwerk-Name: " << this->Lese_NetzwerkName() << endl;

    for( int i=0 ; i < this->Anzahl_NeTEx_Dateien ; i++ )
        cout << i << ". NeTEx-Datei: " << this->Lese_NeTEx_Dateiname_ID(i) << endl;

    cout << "gespeicherte Stopplace-Instanzen: " << this->Lese_Anzahl_Netex_Stopplaces() << endl;

    class NETEX_STOPPLACE TMP;
    for( int i=0 ; i < this->Anzahl_NeTEx_Stopplaces ; i++ )
    {
        TMP = this->Lese_StopPlace_ID(i);
        cout << "StopPlace " << i << ":"
                << "\t" << "ID= " << TMP.Lese_ID()
                << "\t" << "DHID-Code= " << TMP.Lese_DHID_Code()
                << "\t" << "Name= " << TMP.Lese_Name()
                << "\t" << "Laengengrad= " << TMP.Lese_Laengengrad()
                << "\t" << "Breitengrad= " << TMP.Lese_Breitengrad()
                << endl;
    }
}

void OEPNV_NETZWERK::zaehle_stopplaces(void)
{
    for( int i=0 ; i < this->Lese_Anzahl_Netex_Dateien() ; i++ )
    {
        cout << "oeffne als n�chsten die Datei " << this->Lese_NeTEx_Dateiname_ID(i) << endl;
    }
}
