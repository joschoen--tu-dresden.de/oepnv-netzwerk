#ifndef OEPNV-NETZWERK_H_INCLUDED
#define OEPNV-NETZWERK_H_INCLUDED

#include <string>               // wir brauchen die Bibliothek string, um mit dem Dateityp string arbeiten
                                // zu k�nnen

#include "netex_stopplace.h"    // Bibliothek zum Arbeiten mit den NeTEx-Stopplace-Objekten

using namespace std;

class OEPNV_NETZWERK {

    // Attribute
    string Netzwerk_Name;       // Name des Netzwerks, z.B. DVB - Stra�enbahn
    string *NeTEx_Datei;        // Array von Strings zum Speichern der Namen der NeTEx-Dateien

    int Anzahl_NeTEx_Dateien;   // Anzahl der Netex-Dateien

    class NETEX_STOPPLACE *Stop_Place;  // Array von NeTEx_STOPPLACE-ObjektInstanzen
    int Anzahl_NeTEx_Stopplaces;    // Anzahl der zu speichernden Stopplace-Instanzen

    // die Methoden k�nnen in allen anderen Programmteilen genutzt werden
    public:

    // Methoden
    OEPNV_NETZWERK();           // Dies ist der Standard-Konstruktor
    ~OEPNV_NETZWERK();          // Dies ist der Standard Destruktor

    void Setze_NetzwerkName(string _Netzwerk_Name);     // setzt den Netzwerknamen
    void Setze_Anzahl_Netex_Dateien(int _Anzahl);       // setzt das Attribut Anzahl_NeTEx_Dateien

    void Setze_Anzahl_NeTEx_Stopplaces(int _Anzahl);    // setzt das Attribut Anzahl_NeTEx_Stopplaces

    string Lese_NetzwerkName(void);                     // setzt den Netzwerknamen
    int Lese_Anzahl_Netex_Dateien(void);                // setzt das Attribut Anzahl_NeTEx_Dateien
    int Lese_Anzahl_Netex_Stopplaces(void);             // setzt das Attribut Anzahl_NeTEx_Stopplaces

    void Setze_NeTEx_Dateiname_ID(int _ID, string _DateiName);  // setzt den Namen von NeTEx-Datei[_ID]
    string Lese_NeTEx_Dateiname_ID(int _ID);            // liefert den Namen von NeTEx-Datei[_ID] zurueck

    void Setze_StopPlace_ID(int _ID, class NETEX_STOPPLACE _SP);  // setzt Stop_Place[_ID] = _SP
    class NETEX_STOPPLACE Lese_StopPlace_ID(int _ID);    // liefert den Inhalt von Stop_Place[_ID] zur�ck
    void Ausgabe_Netzwerk_Informationen(void);          // druckt alle gespeicherten Informationen am Bildschirm aus

    void zaehle_stopplaces(void);
};

#endif // OEPNV-NETZWERK_H_INCLUDED
