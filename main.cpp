#include <iostream>

#include "oepnv-netzwerk.h"

using namespace std;

int main()
{
    // Reserviere Speicher für eine Instanz des Typs OEPNV_NETZWERK
    class OEPNV_NETZWERK *NETZ;

    // Erzeuge eine Instanz des Typs OEPNV_NETZWERK
    NETZ = new class OEPNV_NETZWERK();

    // Spezifiziere des Namen des Netzwerks
    NETZ->Setze_NetzwerkName("DVB-Straßenbahnen");

    // Lege die Anzahl der zu berücksichtigenden NeTEx-Dateien fest
    NETZ->Setze_Anzahl_Netex_Dateien(12);

    // spezifiziere die NeTEx-Dateien
    NETZ->Setze_NeTEx_Dateiname_ID(0,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-10_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(1,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-11_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(2,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-12_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(3,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-13_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(4,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-14_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(5,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-15_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(6,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-16_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(7,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-5_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(8,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-6_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(9,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-7_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(10,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-8_20230327.xml");
    NETZ->Setze_NeTEx_Dateiname_ID(11,"..\\..\\voe_DVBTra\\NX-PI-01_DE_NAP_LINE_voe-DVBTra-9_20230327.xml");

    NETZ->zaehle_stopplaces();

    // setze die Anzahl der zu speichernden Stopplaces
    //NETZ->Setze_Anzahl_NeTEx_Stopplaces(2);

    // setze die Werte der Stopplaces
    /*
    class NETEX_STOPPLACE *Tmp;
    Tmp = new class NETEX_STOPPLACE("DE::StopPlace:1_voe::","de:14612:1","Dresden Bahnhof Mitte",13.723392,51.055642);
    NETZ->Setze_StopPlace_ID(0, *Tmp);
    Tmp = new class NETEX_STOPPLACE("DE::StopPlace:4_voe::","de:14612:4","Dresden Altmarkt",13.738511,51.050249);
    NETZ->Setze_StopPlace_ID(1, *Tmp);
    */
    // gebe alle gespeicherten Informationen am Bildschirm aus
    NETZ->Ausgabe_Netzwerk_Informationen();

    // lösche die Netzwerk-Objekt-Instanz
    delete NETZ;

    return 0;
}
